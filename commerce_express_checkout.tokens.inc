<?php
/**
 * @file
 * Token replacements for the commerce_express_checkout module.
 *
 * Example testing:
 * drush eval "print token_replace('[commerce-product:url-express-checkout-link]', array('commerce-product' => commerce_product_load(15)));"
 * drush eval "print token_replace('[commerce-product:url-express-checkout-link:15]');"
 */

/**
 * Implements hook_token_info().
 */
function commerce_express_checkout_token_info() {
  // Tokens for products.
  $product = array();
  $product['url-express-checkout-link'] = array(
    'name' => t('Commerce Express Checkout link'),
    'description' => t('A link for Express Checkout for specific product based on product ID or SKU.'),
    'dynamic' => TRUE,
  );

  return array(
    'tokens' => array('commerce-product' => $product),
  );

}

/**
 * Implements hook_tokens().
 */
function commerce_express_checkout_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'commerce-product') {
    // Populate dynamic token.
    if ($key_tokens = token_find_with_prefix($tokens, 'url-express-checkout-link')) {
      foreach ($key_tokens as $key => $original) {
        $product_id = is_string($key) ? commerce_product_load_by_sku($key)->product_id : $key;
        $replacements[$original] = commerce_express_checkout_generate_url($product_id);
      }
    }
    elseif (!empty($data['commerce-product'])) {
      // Handle simple tokens.
      foreach ($tokens as $key => $original) {
        if ($key == 'url-express-checkout-link') {
          $replacements[$original] = commerce_express_checkout_generate_url($data['commerce-product']->product_id);
        }
      }
    }
  }
  return $replacements;
}
