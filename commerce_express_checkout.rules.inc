<?php

/**
 * @file
 * Rules integration for express checkouts.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_express_checkout_rules_event_info() {
  $events = array();

  $events['commerce_express_checkout_product_prepare'] = array(
    'label' => t('Before adding a product to the express checkout'),
    'group' => t('Commerce Cart'),
    'variables' => commerce_cart_rules_event_variables(),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_express_checkout_product_prepare'] = array(
    'label' => t('Before adding a product to the cart'),
    'group' => t('Commerce Cart'),
    'variables' => commerce_cart_rules_event_variables(),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_express_checkout_product_add'] = array(
    'label' => t('After adding a product to the express checkout'),
    'group' => t('Commerce Express Checkout'),
    'variables' => commerce_cart_rules_event_variables(TRUE),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

/**
 * @}
 */
