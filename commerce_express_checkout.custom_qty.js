(function($) {

Drupal.behaviors.cec_custom_qty = {
  attach: function(context, settings) {
    $(':input.qty_ctl', context).on('keydown keyup click input submit mouseenter', function(ev) {
      this.value = this.value.replace(/[^0-9\.]/, '');
      if (this.value && this.value > 0) {
        var id = $(this).attr('id');
        $('a.' + id).attr('href', $('a.' + id).attr('href').replace(/\/\d+$/, '/' + this.value));
      }
    });
  }
};

})(jQuery);
